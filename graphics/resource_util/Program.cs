﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace resource_util
{
    class Program
    {

        static void Main(string[] args)
        {
            if (Directory.Exists(@"C:\Development\sadboy\whodat\whodat\res\drawable-hdpi"))
                new DirectoryInfo(@"C:\Development\sadboy\whodat\whodat\res\drawable-hdpi").Delete(true);
           
            if (Directory.Exists(@"C:\Development\sadboy\whodat\whodat\res\drawable-ldpi"))
                new DirectoryInfo(@"C:\Development\sadboy\whodat\whodat\res\drawable-ldpi").Delete(true);
           
            if (Directory.Exists(@"C:\Development\sadboy\whodat\whodat\res\drawable-mdpi"))
                new DirectoryInfo(@"C:\Development\sadboy\whodat\whodat\res\drawable-mdpi").Delete(true);

            var d1 = Directory.CreateDirectory(@"C:\Development\sadboy\whodat\whodat\res\drawable-hdpi");
            var d2 = Directory.CreateDirectory(@"C:\Development\sadboy\whodat\whodat\res\drawable-ldpi");
            var d3 = Directory.CreateDirectory(@"C:\Development\sadboy\whodat\whodat\res\drawable-mdpi");

            string template = File.ReadAllText(Path.Combine(Directory.GetCurrentDirectory(), "template.txt"));

            var di = new DirectoryInfo(@"C:\Development\sadboy\whodat\whodat\res\drawable");
            foreach (var f in di.GetFiles())
            {
                string name = f.Name.Replace(f.Extension, "");
                string text = string.Format(template, name);

                if (f.Extension != ".png")
                {
                    f.CopyTo(Path.Combine(d1.FullName, f.Name));
                    f.CopyTo(Path.Combine(d2.FullName, f.Name));
                    f.CopyTo(Path.Combine(d3.FullName, f.Name));
                    continue;
                }

                File.Create(Path.Combine(d1.FullName, name + ".xml")).Close();
                File.WriteAllText(Path.Combine(d1.FullName, name + ".xml"), text);

                File.Create(Path.Combine(d2.FullName, name + ".xml")).Close();
                File.WriteAllText(Path.Combine(d2.FullName, name + ".xml"), text);

                File.Create(Path.Combine(d3.FullName, name + ".xml")).Close();
                File.WriteAllText(Path.Combine(d3.FullName, name + ".xml"), text);

            }
        }
    }
}
