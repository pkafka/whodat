package com.sadboy.whodat;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.sadboy.whodat.GameInfo.GameStatus;

public class GameOver extends WhodatBase {

	public final static String WON = "WON";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.game_over);
		
		boolean won = getIntent().getBooleanExtra(WON, true);
		
	    ImageView img = (ImageView)findViewById(R.id.imgTheirs);
	    img.setImageResource(mGame.oponentsCharacter);
		
		TextView tv = (TextView)findViewById(R.id.txtTitle);
		tv.setTypeface(mGame.fontBacklack);
		
		if (won) {
			tv.setText("You Win!");
			
			mGame.playSound(mGame.SOUND_RESID_applause);
	    	mGame.status = GameStatus.GAME_OVER;
	    	
	    	performHapticFeedback(
	    			new long[] {100,200,100,100,100,100,100,100,100,100,100,300,100,300});
	    	
			Intent i = new Intent(GameOver.this, BalloonFill.class);
			startActivityForResult(i, BalloonFill.BALLOON_FILL);
		}
		else {
			performHapticFeedback(
	    			new long[] {0,300,100,300,100,300,100,300,100,2500});
			mGame.playSound(mGame.SOUND_RESID_lose);
			tv.setText("Game Over");
		}
		
		
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		finish();
	}
}
