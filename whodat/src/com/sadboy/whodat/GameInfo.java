package com.sadboy.whodat;

import java.util.ArrayList;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Handler;
import android.os.Message;
import android.widget.Toast;

import com.sadboy.whodat.AppMessage.AppMessageType;
import com.sadboy.whodat.Family.WhodatChar;


public class GameInfo extends android.app.Application  {
	
	public final static String SHARED_PREFS = "com.sadboy.whodat";
	public final static String SETTINGS_SOUND = "sound";
	
	public enum GameType{
		ONE_PLAYER,
		TWO_PLAYER,
		CLASSIC
	}
	
	public enum LinkedRole{
		HOST,
		CLIENT
	}
	
	public enum GameTurn{
		MINE,
		OPPONENTS
	}
	
	public enum GameStatus{
		PLAYING,
		NEW,
		GAME_OVER
	}

	public BluetoothManager btManager;
	public Family chosenFamily;
	public GameType type;
	public LinkedRole linkedAs;
	public GameTurn turn;
	public GameStatus status;
	public int chosenCharacter;
	public int oponentsCharacter;
	public boolean autoFlip;
	
	GameGallery gameGallery;
	
	SoundPool soundPool;
	SharedPreferences prefs;
	
	

	public int SOUND_RESID_applause;
	public int SOUND_RESID_balloon_stretch_pop;
	public int SOUND_RESID_balloon_deflate;
	public int SOUND_RESID_balloon_pop;
	public int SOUND_RESID_bell;
	public int SOUND_RESID_buzz;	
	public int SOUND_RESID_lose;
	public int SOUND_RESID_poing;
	public int SOUND_RESID_water_balloon;
	public int SOUND_RESID_splat;
	public int SOUND_RESID_swoosh;
	
	private MediaPlayer mSoundMainMusic;
	MediaPlayer soundAirplane;
	
	Typeface fontBacklack;
	
	private boolean mHasQuit;
	
	@Override
	public void onCreate() {
		super.onCreate();
		create();
	}
	
	private void create(){
		btManager = new BluetoothManager(this, mHandler);
		
		prefs = getSharedPreferences(SHARED_PREFS, 0);
		autoFlip = true;
		status = GameStatus.NEW;
		
		fontBacklack = Typeface.createFromAsset(
				getAssets(), "fonts/backlash.ttf");
		
		mSoundMainMusic = MediaPlayer.create(this, 
				R.raw.main_scheming_weasel_faster);
		
		soundAirplane = MediaPlayer.create(this, R.raw.airplane);
		isSoundEnabled();
		
		soundPool = new SoundPool(15, AudioManager.STREAM_MUSIC, 0);
		SOUND_RESID_applause = soundPool.load(this, R.raw.applause, 1);
		SOUND_RESID_balloon_stretch_pop = soundPool.load(this, R.raw.balloon_stretch_pop, 1);
		SOUND_RESID_balloon_deflate = soundPool.load(this, R.raw.balloon_deflate, 1);
		SOUND_RESID_balloon_pop = soundPool.load(this, R.raw.balloon_pop, 1);
		SOUND_RESID_bell = soundPool.load(this, R.raw.bell, 1);
		SOUND_RESID_buzz = soundPool.load(this, R.raw.buzz, 1);
		SOUND_RESID_lose = soundPool.load(this, R.raw.lose, 1);
		SOUND_RESID_poing = soundPool.load(this, R.raw.poing, 1);
		SOUND_RESID_water_balloon = soundPool.load(this, R.raw.water_balloon, 1);
		SOUND_RESID_splat = soundPool.load(this, R.raw.splat, 1);
		SOUND_RESID_swoosh = soundPool.load(this, R.raw.swoosh, 1);
	}
	
	public void playSound(int sound){
		try{
		if (!prefs.getBoolean(GameInfo.SETTINGS_SOUND, true)) return;
		else {
			if (sound == SOUND_RESID_bell || 
					sound == SOUND_RESID_buzz ||
					sound == SOUND_RESID_balloon_stretch_pop ||
					sound == SOUND_RESID_balloon_deflate){
				soundPool.play(sound, 0.6f, 0.6f, 1, 0, 1.0f);
			}else soundPool.play(sound, 1.0f, 1.0f, 1, 0, 1.0f);
		}
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	public void pauseAllSound(int streamId){
		soundPool.pause(streamId);
	}
	
	public TypedArray getCharacters(int questionId){
		switch (questionId){
			case R.drawable.question_female:
				return getResources().obtainTypedArray(R.array.female);
			case R.drawable.question_glasses:
				return getResources().obtainTypedArray(R.array.glasses);
			case R.drawable.question_hair_bald:
				return getResources().obtainTypedArray(R.array.bald);
			case R.drawable.question_hair_black:
				return getResources().obtainTypedArray(R.array.hair_black);
			case R.drawable.question_hair_blonde:
				return getResources().obtainTypedArray(R.array.hair_blonde);
			case R.drawable.question_hair_brown:
				return getResources().obtainTypedArray(R.array.hair_brown);
			case R.drawable.question_hair_red:
				return getResources().obtainTypedArray(R.array.hair_red);
			case R.drawable.question_hair_white:
				return getResources().obtainTypedArray(R.array.hair_gray);
			case R.drawable.question_hat:
				return getResources().obtainTypedArray(R.array.hat);
			case R.drawable.question_male:
				return getResources().obtainTypedArray(R.array.male);
			case R.drawable.question_mustache:
				return getResources().obtainTypedArray(R.array.mustache);
			case R.drawable.question_beard:
				return getResources().obtainTypedArray(R.array.beard);
			case R.drawable.question_skin_dark:
				return getResources().obtainTypedArray(R.array.skin_dark);
			case R.drawable.question_skin_light:
				return getResources().obtainTypedArray(R.array.skin_light);
			case R.drawable.question_hair_curly:
				return getResources().obtainTypedArray(R.array.hair_curly);
			case R.drawable.question_tie:
				return getResources().obtainTypedArray(R.array.tie);
		}
		return null;
	}
	
	
	boolean hasFeature(int characterId, TypedArray charsWithFeatuer){
		for (int i = 0; i < charsWithFeatuer.length(); i++) {
			if (characterId == charsWithFeatuer.getResourceId(i, 1)) return true;
		}
		return false;
	}
	
	public boolean isSoundEnabled(){
		boolean enabled = prefs.getBoolean(GameInfo.SETTINGS_SOUND, true);
		try{
		if (enabled){
			if (mSoundMainMusic == null || 
					!mSoundMainMusic.isPlaying()){
				mSoundMainMusic = MediaPlayer.create(this, 
						R.raw.main_scheming_weasel_faster);
				mSoundMainMusic.setLooping(true);
				mSoundMainMusic.start();
				
				soundAirplane.setVolume(1.0f, 1.0f);
			}
		}
		else{
			if (mSoundMainMusic != null) mSoundMainMusic.stop();
			if (mSoundMainMusic != null) mSoundMainMusic.release();
			mSoundMainMusic = null;
			soundAirplane.setVolume(0.0f, 0.0f);
		}
		}catch(Exception ex){}
		return enabled;
	}
	public boolean toggleSoundEnabled(){
		SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(GameInfo.SETTINGS_SOUND, 
        		!prefs.getBoolean(GameInfo.SETTINGS_SOUND, true));
        editor.commit();
        return prefs.getBoolean(GameInfo.SETTINGS_SOUND, true);
	}
	
	void quit(){
		mHasQuit = true;
		try{
			if (mSoundMainMusic != null)mSoundMainMusic.stop();
			if (soundAirplane != null)soundAirplane.stop();
			btManager.stop();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
    void pauseMusic(){
    	try{
    	if (mSoundMainMusic != null) mSoundMainMusic.pause();
    	if (soundAirplane != null) {
    		soundAirplane.stop();
    		soundAirplane.seekTo(0);
    	}
    	}
    	catch(Exception ex){
    		ex.printStackTrace();
    	}
    }
    void resumeMusic(){
    	if (mSoundMainMusic != null) mSoundMainMusic.start();
    }
    
    /**
     * Sends a message.
     * @param message  A string of text to send.
     */
    void sendMessage(AppMessage message) {
        // Check that we're actually connected before trying anything
        if (btManager.getState() != BluetoothManager.STATE_CONNECTED) {
            return;
        }
        
        if (message.type == AppMessageType.USER_MESSAGE){
            btManager.conversationArrayAdapter.add(
            		"Me:  " + message.data);
            btManager.conversationArrayAdapter.notifyDataSetChanged();
        }
        
        byte[] send = message.toString().getBytes();
        btManager.write(send);
    }
	
 // The Handler that gets information back from the BluetoothManager
    private final Handler mHandler = new Handler() {

		@Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
            case BluetoothManager.MESSAGE_STATE_CHANGE:
                switch (msg.arg1) {
                case BluetoothManager.STATE_CONNECTED:
                	playSound(SOUND_RESID_buzz);
                    btManager.conversationArrayAdapter.clear();
                    type = GameType.TWO_PLAYER;
    				turn = GameTurn.MINE;
                	Intent i = new Intent(getApplicationContext(), ChooseCharacterGallery.class);
                	i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    				startActivity(i);
                    break;
                case BluetoothManager.STATE_CONNECTING:
                    //mTitle.setText(R.string.title_connecting);
                    break;
                case BluetoothManager.STATE_LISTEN:
                case BluetoothManager.STATE_NONE:
                    //mTitle.setText(R.string.title_not_connected);
                    break;
                }
                break;
            case BluetoothManager.MESSAGE_WRITE:
                byte[] writeBuf = (byte[]) msg.obj;
                // construct a string from the buffer
                String writeMessage = new String(writeBuf);
                break;
            case BluetoothManager.MESSAGE_READ:
                byte[] readBuf = (byte[]) msg.obj;
                // construct a string from the valid bytes in the buffer
                String readMessage = new String(readBuf, 0, msg.arg1);
                AppMessage m = AppMessage.getFromString(readMessage);
                playSound(SOUND_RESID_poing);
                switch (m.type){
                case CHARACTER_CHOSEN:
                	oponentsCharacter = Integer.parseInt(m.data);
    				Toast.makeText(getApplicationContext(), "Opponent Ready!", Toast.LENGTH_LONG).show();
                	break;
                case USER_MESSAGE:
                	btManager.conversationArrayAdapter.add(
                    		btManager.connectedDeviceName + ":  " + m.data);
                	btManager.conversationArrayAdapter.notifyDataSetChanged();
                	Toast.makeText(getApplicationContext(), 
                			btManager.connectedDeviceName + ": " + m.data, 
                			Toast.LENGTH_LONG).show();
                	break;
                case OPPONENT_WON:
                	if (gameGallery != null) gameGallery.doLose(false);
                	break;
                case OPPONENT_FORFEIT:
                	if (gameGallery != null) gameGallery.doWin(false);
                	break;
                }
                break;
            case BluetoothManager.MESSAGE_DEVICE_NAME:
                // save the connected device's name
                btManager.connectedDeviceName = 
                	msg.getData().getString(BluetoothManager.DEVICE_NAME);
                Toast.makeText(getApplicationContext(), "Connected to "
                               + btManager.connectedDeviceName, Toast.LENGTH_SHORT).show();
                break;
            case BluetoothManager.MESSAGE_TOAST:
                Toast.makeText(getApplicationContext(), 
                		msg.getData().getString(BluetoothManager.TOAST),
                               Toast.LENGTH_SHORT).show();
                break;
            }
        }
    };
    
}

