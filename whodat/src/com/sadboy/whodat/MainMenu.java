package com.sadboy.whodat;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.ads.AdRequest;
import com.google.ads.AdView;
import com.sadboy.whodat.GameInfo.GameTurn;
import com.sadboy.whodat.GameInfo.GameType;

public class MainMenu extends WhodatBase {
   
	protected static final int DIALOG_QUIT = 10;
	
    private View mbtnClassic;
    private View mbtnLinked;
    private View mbtnLinked2;
    private View mbtnOnePlayer;
    private View mTicker;
    private Animation mFadeInAnimation;
    private AdView mAdView;
    
    protected static final int REQUEST_CONNECT_DEVICE = 122;
    protected static final int REQUEST_ENABLE_BT = 123;
    
    @Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		if (!hasFocus)return;
		
		//startAnim((ImageView)findViewById(R.id.imgBilboard));
		startAnim((ImageView)findViewById(R.id.imgChef));
		startAnim((ImageView)findViewById(R.id.imgTyrell));
		startAnim((ImageView)findViewById(R.id.imgBetsy));
	}
    
    void startAnim(ImageView img){
    	if (img == null)return;
    	img.setVisibility(View.VISIBLE);
    	AnimationDrawable ad = (AnimationDrawable)img.getBackground();
		ad.start();
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mainmenu);
        
        mTicker = findViewById(R.id.ticker);
        
        mAdView = (AdView)findViewById(R.id.ad);
        mAdView.setVisibility(View.VISIBLE);
        
        mFadeInAnimation = AnimationUtils.loadAnimation(this, R.anim.fade_in);
        
        mbtnClassic = findViewById(R.id.btnClasic);
        mbtnClassic.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
					performHapticFeedback();
					Animation anim = AnimationUtils.loadAnimation(MainMenu.this, R.anim.deflate);
					mbtnClassic.clearAnimation();
					anim.setAnimationListener(new AnimationListener() {
						@Override
						public void onAnimationStart(Animation animation) {}
						@Override
						public void onAnimationRepeat(Animation animation) {}
						@Override
						public void onAnimationEnd(Animation animation) {
							mbtnClassic.setVisibility(View.INVISIBLE);
							Intent i = new Intent(MainMenu.this, ChooseCharacterGallery.class);
			                mGame.type = GameType.CLASSIC;
			                startActivity(i);
						}
					});
					mbtnClassic.startAnimation(anim);
					mGame.playSound(mGame.SOUND_RESID_balloon_deflate);
			}
		});
        
        mbtnLinked = findViewById(R.id.btnLinked);
        mbtnLinked2 = findViewById(R.id.btnLinked2);
        mbtnLinked.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				performHapticFeedback();
				mGame.playSound(mGame.SOUND_RESID_splat);
				
				if (!mGame.btManager.bluetoothExists()) {
	                Toast.makeText(MainMenu.this, "Bluetooth is not available", 
	                		Toast.LENGTH_LONG).show();
	                return;
	            }
	            if (!mGame.btManager.bluetoothEnabled()) {
	                Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
	                startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
	                return;
	            }
	            
	            if (mGame.btManager.getState() != BluetoothManager.STATE_CONNECTED){
		            mGame.type = GameType.TWO_PLAYER;
					mGame.turn = GameTurn.MINE;
		            Intent serverIntent = new Intent(MainMenu.this, DeviceListActivity.class);
		            startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
	            }
	            else{
	            	Intent i = new Intent(MainMenu.this, ChooseCharacterGallery.class);
	                mGame.type = GameType.TWO_PLAYER;
	                startActivity(i);
	            }
			}
        });
        
        mbtnOnePlayer = findViewById(R.id.btnOnePlayer);
        mbtnOnePlayer.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
					performHapticFeedback();
					Animation anim = AnimationUtils.loadAnimation(MainMenu.this, R.anim.grow);
					mbtnOnePlayer.clearAnimation();
					anim.setAnimationListener(new AnimationListener() {
						@Override
						public void onAnimationStart(Animation animation) {}
						@Override
						public void onAnimationRepeat(Animation animation) {}
						@Override
						public void onAnimationEnd(Animation animation) {
							mbtnOnePlayer.setVisibility(View.INVISIBLE);
							Intent i = new Intent(MainMenu.this, ChooseCharacterGallery.class);
			                mGame.type = GameType.ONE_PLAYER;
			                startActivity(i);
						}
					});
					mGame.playSound(mGame.SOUND_RESID_balloon_stretch_pop);
					mbtnOnePlayer.startAnimation(anim);
			}
        });
    }
    
    @Override
    protected void onStart() {
    	super.onStart();
    	mAdView.loadAd(new AdRequest());
    }
    
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
        case REQUEST_ENABLE_BT:
            if (resultCode == Activity.RESULT_OK){
            	Intent serverIntent = new Intent(MainMenu.this, DeviceListActivity.class);
	            startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
            }
            break;
        case REQUEST_CONNECT_DEVICE:
            // When DeviceListActivity returns with a device to connect
            if (resultCode == Activity.RESULT_OK) {
                // Get the device MAC address
                String address = data.getExtras().getString(
                		DeviceListActivity.EXTRA_DEVICE_ADDRESS);
                // Attempt to connect to the device
                mGame.btManager.connect(address);
            }
            break;
        case DIALOG_QUIT:
			if (resultCode == Quit.RESULTS_YES){
				mGame.quit();
				finish();
			}
			break;
		}
    }
    
    @Override
    protected void onDestroy() {
    	mGame.quit();
    	super.onDestroy();
    }
    
    @Override
	public void onBackPressed() {
		if (mSettingsMenu != null && mSettingsMenu.isOpened()){
			mSettingsMenu.animateClose();
		}
		else {
			Intent i = new Intent(MainMenu.this, Quit.class);
			startActivityForResult(i, DIALOG_QUIT);
		}
	}
    
    @Override
	public void onResume() {
        super.onResume();
        if (mbtnClassic != null) {
        	mbtnClassic.clearAnimation();
        	Animation anim = AnimationUtils.loadAnimation(this, R.anim.float_up);
        	mbtnClassic.startAnimation(anim);
        }
        if (mbtnOnePlayer != null) {
        	mbtnOnePlayer.clearAnimation();
            Animation anim = AnimationUtils.loadAnimation(this, R.anim.float_up);
            anim.setStartOffset(500L);
            mbtnOnePlayer.startAnimation(anim);
        }
        if (mbtnLinked != null) {
        	mbtnLinked.clearAnimation();
            Animation anim = AnimationUtils.loadAnimation(this, R.anim.float_up);
            anim.setStartOffset(1200L);
            mbtnLinked.startAnimation(anim);
            
            mbtnLinked2.clearAnimation();
            Animation anim2 = AnimationUtils.loadAnimation(this, R.anim.float_up);
            anim2.setStartOffset(250);
            mbtnLinked2.startAnimation(anim2);
        }
        if (mTicker != null) {
        	mTicker.clearAnimation();
        	mTicker.setAnimation(mFadeInAnimation);
        }
        
        mAdView.loadAd(new AdRequest());
    }


	

}
