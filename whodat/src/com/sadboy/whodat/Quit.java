package com.sadboy.whodat;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class Quit extends WhodatBase {
	
	public final static int RESULTS_NO = 102;
	public final static int RESULTS_YES = 202;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.quit);
		
		TextView tvq = (TextView)findViewById(R.id.TextView01);
		tvq.setTypeface(mGame.fontBacklack);
		
		TextView tvyes = (TextView)findViewById(R.id.txtYes);
		tvyes.setTypeface(mGame.fontBacklack);
		tvyes.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				setResult(RESULTS_YES);
				finish();
			}
		});
		
		TextView tvNo = (TextView)findViewById(R.id.txtNo);
		tvNo.setTypeface(mGame.fontBacklack);
		tvNo.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				setResult(RESULTS_NO);
				finish();
			}
		});
	}
	
	@Override
	public void onBackPressed() {
	}
}
