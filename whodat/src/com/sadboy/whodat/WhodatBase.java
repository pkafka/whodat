package com.sadboy.whodat;

import java.util.Random;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.drawable.AnimationDrawable;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SlidingDrawer;
import android.widget.TextView;

import com.google.ads.AdListener;
import com.google.ads.AdRequest;
import com.google.ads.AdView;

public abstract class WhodatBase extends Activity implements AnimationListener {

    protected GameInfo mGame;
    long mLastTouched;
    boolean enableAdds = true;
    private AdView mAirplaneAd;
    private View mAirplaneView;
    private Animation mAirplaneAnimation;
    private Handler mHandler = new Handler();
    protected SlidingDrawer mSettingsMenu;
    private Vibrator mVibrator;
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mGame = (GameInfo)getApplication();
		mVibrator = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
		setVolumeControlStream(AudioManager.STREAM_MUSIC);
	}
	
	public void initSettingsMenu(){
		if (mSettingsMenu != null) return;
		
		mSettingsMenu = (SlidingDrawer)findViewById(R.id.drawer);

		ImageView imgSound = (ImageView)findViewById(R.id.imgSound);
		if (imgSound == null)return;
		
        if (mGame.isSoundEnabled()){
        	imgSound.setImageResource(R.drawable.sound_on);
        }else imgSound.setImageResource(R.drawable.sound_off);
        imgSound.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				performHapticFeedback();
				mGame.toggleSoundEnabled();
		        ImageView imgSound = (ImageView)findViewById(R.id.imgSound);
		        if (mGame.isSoundEnabled()){
		        	imgSound.setImageResource(R.drawable.sound_on);
		        }else{
		        	imgSound.setImageResource(R.drawable.sound_off);
		        }
			}
		});
        
        ImageView imgInfo = (ImageView)findViewById(R.id.imgInfo);
        imgInfo.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				performHapticFeedback();
				Intent i = new Intent(WhodatBase.this, Credits.class);
				startActivity(i);
			}
		});
	}
	
	protected void performHapticFeedback(){
		mVibrator.vibrate(50);
	}
	
	protected void performHapticFeedback(long[] pattern){
		mVibrator.vibrate(pattern, -1);
	}
	
	private void setupAnimation(){
		
		View background = (RelativeLayout)findViewById(R.id.mainMenuBackground);
		if (background != null){
			AnimationDrawable d = (AnimationDrawable)background.getBackground();
			if (d != null)d.start();
		}
	       
	    ImageView config = (ImageView)findViewById(R.id.handle);
	    if (config != null) {
	    	AnimationDrawable d = (AnimationDrawable)config.getBackground();
	    	if (d != null)d.start();
	    }
	       
        mHandler.postDelayed(mUpdateTimeTask, 
        		new Random().nextInt(60) * 1000);
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		initSettingsMenu();
		if (mSettingsMenu != null){
			if (mSettingsMenu.isOpened()) mSettingsMenu.animateClose();
			else mSettingsMenu.animateOpen();
		}
		return true;
	}
	
    @Override
	public void onBackPressed() {
    	initSettingsMenu();
    	if (mSettingsMenu != null && mSettingsMenu.isOpened()){
			mSettingsMenu.animateClose();
		}
    	else super.onBackPressed();
	}
	
	private Runnable mUpdateTimeTask = new Runnable() {
	   public void run() {
		   runOnUiThread(doStartAirplane);
		}
	};
	private Runnable doStartAirplane = new Runnable() {
	   public void run() {
		   if (mAirplaneAd == null){
				LayoutInflater l = getLayoutInflater();
				mAirplaneView = l.inflate(R.layout.airplane_ad, null);
		        
			    ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(
					   ViewGroup.LayoutParams.FILL_PARENT, 
					   ViewGroup.LayoutParams.FILL_PARENT);
			    addContentView(mAirplaneView, layoutParams);
			    
			    mAirplaneView.setVisibility(View.GONE);
			    mAirplaneAd = (AdView)mAirplaneView.findViewById(R.id.ad);
			    mAirplaneAd.loadAd(new AdRequest());
			    
	            mAirplaneAnimation = new TranslateAnimation(
	            		-500,400, 0.0f, 0.0f);
	            mAirplaneAnimation.setInterpolator(
	            		AnimationUtils.loadInterpolator(
	            				WhodatBase.this, android.R.anim.linear_interpolator));
	            mAirplaneAnimation.setDuration(15000);
	            mAirplaneAnimation.setAnimationListener(WhodatBase.this);
		   }
		   if (mAirplaneAd.isReady()){
			   mAirplaneView.setVisibility(View.VISIBLE);
			   mAirplaneView.startAnimation(mAirplaneAnimation);
			   if (mGame.isSoundEnabled()) mGame.soundAirplane.start();
		   }else {
			   mAirplaneAd.loadAd(new AdRequest());
			   mHandler.postDelayed(mUpdateTimeTask, 4500);
		   }
		}
	};
	
	void showTip(String tip){
		LayoutInflater l = getLayoutInflater();
		final View view = l.inflate(R.layout.tip, null, false);
		
		TextView tv1 = (TextView)view.findViewById(R.id.textView1);
		tv1.setTypeface(mGame.fontBacklack);
		
		TextView tv2 = (TextView)view.findViewById(R.id.textView2);
		tv2.setTypeface(mGame.fontBacklack);
		
		final TextView tvTip = (TextView)view.findViewById(R.id.txtTip);
		tvTip.setTypeface(mGame.fontBacklack);
		tvTip.setText(tip);
		
		ImageView img = (ImageView)view.findViewById(R.id.imgNextTip);
		img.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				tvTip.setText(nextTip());
			}
		});
		ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(
				   ViewGroup.LayoutParams.WRAP_CONTENT, 
				   ViewGroup.LayoutParams.WRAP_CONTENT);
		    addContentView(view, layoutParams);
		    int w = view.getWidth();
		    int h = view.getHeight();
		    //v.layout(left, top, v.getWidth(), v.getHeight());
		    
	    Animation a = new TranslateAnimation(
        		0.0f,0.0f, 400f, 0.0f);
        a.setInterpolator(AnimationUtils.loadInterpolator(
        		WhodatBase.this, android.R.anim.overshoot_interpolator));
        a.setDuration(1000);
        view.startAnimation(a);
        
        tvTip.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				view.setVisibility(View.GONE);
			}
		});
        RelativeLayout back = (RelativeLayout)view.findViewById(R.id.relativeLayout1);
        back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				view.setVisibility(View.GONE);
			}
		});
	}
	
	String nextTip(){
    	TypedArray ar = null;
    	String nextTip = "";
		switch (mGame.type){
		case CLASSIC:
			ar = getResources().obtainTypedArray(R.array.tips_classic_gg);
			break;
		case ONE_PLAYER:
			ar = getResources().obtainTypedArray(R.array.tips_1p_gg);
			break;
		case TWO_PLAYER:
			ar = getResources().obtainTypedArray(R.array.tips_2p_gg);
		}
		if (ar != null){
			Random r = new Random();
			int resId = ar.getResourceId(r.nextInt(ar.length()), -1);
			nextTip = getString(resId);
			ar.recycle();
		}
		return nextTip;
    }
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		mLastTouched = System.currentTimeMillis();
		return super.onTouchEvent(event);
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		mHandler.removeCallbacks(mUpdateTimeTask);
		mGame.pauseMusic();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		mHandler.postDelayed(mUpdateTimeTask, new Random().nextInt(60) * 1000);
	}
	
	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		if (!hasFocus)return;
		mGame.resumeMusic();
		initSettingsMenu();
		setupAnimation();
	}
    
    Animation getFadeAnim(){
    	Animation a = AnimationUtils.loadAnimation(WhodatBase.this, R.anim.fade);
    	a.setRepeatCount(Animation.INFINITE);
    	a.setRepeatMode(Animation.REVERSE);
    	return a;
    }
    
    @Override
    protected void onDestroy() {
        super.onDestroy();
		mHandler.removeCallbacks(mUpdateTimeTask);
    }
   
   @Override
   protected void onSaveInstanceState(Bundle outState) {
   	super.onSaveInstanceState(outState);
   }
	
	

	@Override
	public void onAnimationEnd(Animation animation) {
		mAirplaneView.setVisibility(View.GONE);
		mAirplaneAd.loadAd(new AdRequest());
		int next = new Random().nextInt(90);
	    mHandler.postDelayed(mUpdateTimeTask, next * 1000);
	}
	@Override
	public void onAnimationRepeat(Animation animation) {}
	@Override
	public void onAnimationStart(Animation animation) {
	}
	
    
}
