package com.sadboy.whodat;

import java.util.Random;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.TextView;

import com.sadboy.whodat.AppMessage.AppMessageType;
import com.sadboy.whodat.Family.WhodatChar;
import com.sadboy.whodat.GameInfo.GameTurn;
import com.sadboy.whodat.GameInfo.GameType;

public class ChooseCharacterGallery extends WhodatBase  {
	
	Gallery mGallery;
	GalleryAdapter mAdapter;
	boolean windowHasFocus;
	
	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		windowHasFocus = hasFocus;
		if (windowHasFocus) new Thread(startAutoScrollTimer).start();
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.choose_character);
		
		mGame.chosenFamily = new Family(this, R.array.family_00);
		
		TextView tv = (TextView)findViewById(R.id.textView1);
		tv.setTypeface(mGame.fontBacklack);

		mGallery = (Gallery)findViewById(R.id.gallery1);
		mAdapter = new GalleryAdapter(mGame.chosenFamily);
		mGallery.setAdapter(mAdapter);
		mGallery.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				
				performHapticFeedback();
				ViewHolder holder = (ViewHolder)arg1.getTag();
		    	mGame.chosenCharacter = holder.id;
		    	mGame.sendMessage(new AppMessage(Integer.toString(holder.id), 
						AppMessageType.CHARACTER_CHOSEN));
		    	
		    	mGame.playSound(mGame.SOUND_RESID_poing);
		    	
		    	if (mGame.type == GameType.ONE_PLAYER){
		    		mGame.turn = GameTurn.MINE;
		    		//computer picks it's character
		    		int index = new Random().nextInt(mGame.chosenFamily.characters.size());
		    		mGame.oponentsCharacter = mGame.chosenFamily.characters.get(index).resId;
		    	}
				
				Intent i = new Intent(ChooseCharacterGallery.this, GameGallery.class);
				startActivity(i);
			}
		});
		mGallery.setOnItemLongClickListener(new OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				ViewHolder holder = (ViewHolder)arg1.getTag();
				Intent i = new Intent(ChooseCharacterGallery.this, CharacterDetail.class);
				i.putExtra(CharacterDetail.CHARACTER_ID, holder.id);
				startActivity(i);
				return true;
			}
		});
		mLastTouched = System.currentTimeMillis();
		mGallery.setSelection(1, true);
	}
	
	Runnable startAutoScrollTimer = new Runnable() {
		@Override
		public void run() {
			try {Thread.sleep(1500);} catch (InterruptedException e) {}
			runOnUiThread(doAutoScroll);
			while (windowHasFocus) {
				try {Thread.sleep(3000);} catch (InterruptedException e) {}
				if (mLastTouched < (System.currentTimeMillis() - 3500)){
					runOnUiThread(doAutoScroll);
				}
			}
		}
	};
	Runnable doAutoScroll = new Runnable() {
		@Override
		public void run() {
			if (mGallery.getSelectedItemPosition() <= mAdapter.getCount()){
				mGallery.onKeyDown(KeyEvent.KEYCODE_DPAD_RIGHT, new KeyEvent(0, 0));
			}
			else if (mGallery.getSelectedItemPosition() >= mAdapter.getCount()){
				mGallery.setSelection(0, true);
			}
		}
	};
	
	class GalleryAdapter extends BaseAdapter {

		private Family mFamily;
		private LayoutInflater mInflater;
		
		public GalleryAdapter(Family f){
	        mInflater = getLayoutInflater();
	        mFamily = f;
		}
		
		@Override
		public int getCount() {
			return mFamily.characters.size();
		}

		@Override
		public Object getItem(int position) {
			return mFamily.characters.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder;
	    	if (convertView == null)
	    		convertView = createView(parent);
	    		
	    	holder = (ViewHolder)convertView.getTag();
	    	
	    	WhodatChar c = mFamily.characters.get(position);
	        holder.id = c.resId;
	        holder.imgMain.setImageResource(holder.id);
	        holder.txtName.setText(c.name);
	        holder.txtNameBack.setText(c.name);
	        return convertView;
	    }
		
		public View createView(ViewGroup parent){
	    	View v = mInflater.inflate(R.layout.character, parent, false);
	    	ViewHolder holder = new ViewHolder();
	    	holder.imgMain = (ImageView)v.findViewById(R.id.imgMain); 
	    	holder.txtName = (TextView)v.findViewById(R.id.txtName);
	    	holder.txtNameBack = (TextView)v.findViewById(R.id.txtNameBack);
	    	holder.txtName.setTypeface(mGame.fontBacklack);
	    	holder.txtNameBack.setTypeface(mGame.fontBacklack);
	    	v.setTag(holder);
	    	return v;
	    }
		
	}
	
	static class ViewHolder{
    	ImageView imgMain;
    	TextView txtName;
    	TextView txtNameBack;
    	int id;
    }
	
}
