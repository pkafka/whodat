package com.sadboy.whodat;

import org.json.JSONException;
import org.json.JSONObject;

public class AppMessage {

	public enum AppMessageType{
		USER_MESSAGE,
		FAMILY_CHOSEN,
		CHARACTER_CHOSEN,
		OPPONENT_WON,
		OPPONENT_FORFEIT
	};
	
	String data;
	AppMessageType type;
	
	AppMessage(String data, AppMessageType type){
		this.data = data;
		this.type = type;
	}
	AppMessage(){}
	
	@Override
	public String toString(){
		JSONObject obj = new JSONObject();
		try {
			obj.put("Data", data);
			obj.put("Type", type.toString());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return obj.toString();
	}
	
	public static AppMessage getFromString(String str){
		AppMessage m = new AppMessage();
		try {
			JSONObject obj = new JSONObject(str);
			m.data = obj.getString("Data");
			m.type = Enum.valueOf(AppMessageType.class, obj.getString("Type"));
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return m;
	}
}


