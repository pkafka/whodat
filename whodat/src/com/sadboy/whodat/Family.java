package com.sadboy.whodat;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import android.content.Context;
import android.content.res.TypedArray;

public class Family {
	
	public int imageId;
	
	public ArrayList<WhodatChar> characters = 
		new ArrayList<Family.WhodatChar>();
	
	Family(){}
	Family(Context c, int imageId){
		this.imageId = imageId;
		inflateFamily(c, this);
	}
	
	public static ArrayList<Family> getAllFamilies(Context c){
		ArrayList<Family> fams = new ArrayList<Family>();
		fams.add(new Family(c, R.drawable.family_00));
		return fams;
	}
	
	private void inflateFamily(Context c, Family f){
		TypedArray typedCharacter = null;
		TypedArray typedNames = null;
		switch (f.imageId){
			case R.drawable.family_00:
			default:
				typedCharacter = c.getResources().obtainTypedArray(R.array.family_00);
				typedNames = c.getResources().obtainTypedArray(R.array.family_00_names);
			break;
		}
		
		for (int i = 0; i < typedCharacter.length(); i++) {
			characters.add(new WhodatChar(
					typedCharacter.getResourceId(i, -1), 
					typedNames.getString(i)));
		}
		Collections.shuffle(characters);
		typedCharacter.recycle();
		typedNames.recycle();
	}
	
	
	
	public class WhodatChar{
		int resId;
		String name;
		WhodatChar(int r, String n){
			resId = r;
			name = n;
		}
	}

}
