package com.sadboy.whodat;

import java.util.Random;

import android.content.res.TypedArray;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.google.ads.Ad;
import com.google.ads.AdRequest.ErrorCode;
import com.sadboy.whodat.GameInfo.GameStatus;

public class BalloonFill extends WhodatBase {
	
	public static final int BALLOON_FILL = 13;

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		if (!hasFocus)return;

		TypedArray balloons = getResources().obtainTypedArray(R.array.random_balloons);

    	Random r = new Random();
    	setupBalloon((ImageView)findViewById(R.id.balloon_01), r, 
    			balloons.getResourceId(r.nextInt(balloons.length()), -1));
    	setupBalloon((ImageView)findViewById(R.id.balloon_02), r, 
    			balloons.getResourceId(r.nextInt(balloons.length()), -1));
    	setupBalloon((ImageView)findViewById(R.id.balloon_03), r, 
    			balloons.getResourceId(r.nextInt(balloons.length()), -1));
    	setupBalloon((ImageView)findViewById(R.id.balloon_04), r, 
    			balloons.getResourceId(r.nextInt(balloons.length()), -1));
    	setupBalloon((ImageView)findViewById(R.id.balloon_05), r, 
    			balloons.getResourceId(r.nextInt(balloons.length()), -1));
    	setupBalloon((ImageView)findViewById(R.id.balloon_06), r, 
    			balloons.getResourceId(r.nextInt(balloons.length()), -1));
    	setupBalloon((ImageView)findViewById(R.id.balloon_07), r, 
    			balloons.getResourceId(r.nextInt(balloons.length()), -1));
    	setupBalloon((ImageView)findViewById(R.id.balloon_08), r, 
    			balloons.getResourceId(r.nextInt(balloons.length()), -1));
    	setupBalloon((ImageView)findViewById(R.id.balloon_09), r, 
    			balloons.getResourceId(r.nextInt(balloons.length()), -1));
    	setupBalloon((ImageView)findViewById(R.id.balloon_10), r, 
    			balloons.getResourceId(r.nextInt(balloons.length()), -1));
    	setupBalloon((ImageView)findViewById(R.id.balloon_11), r, 
    			balloons.getResourceId(r.nextInt(balloons.length()), -1));
    	setupBalloon((ImageView)findViewById(R.id.balloon_12), r, 
    			balloons.getResourceId(r.nextInt(balloons.length()), -1));
    	setupBalloon((ImageView)findViewById(R.id.balloon_13), r, 
    			balloons.getResourceId(r.nextInt(balloons.length()), -1));
    	setupBalloon((ImageView)findViewById(R.id.balloon_14), r, 
    			balloons.getResourceId(r.nextInt(balloons.length()), -1));
    	setupBalloon((ImageView)findViewById(R.id.balloon_15), r, 
    			balloons.getResourceId(r.nextInt(balloons.length()), -1));
    	setupBalloon((ImageView)findViewById(R.id.balloon_16), r, 
    			balloons.getResourceId(r.nextInt(balloons.length()), -1));
    	setupBalloon((ImageView)findViewById(R.id.balloon_17), r, 
    			balloons.getResourceId(r.nextInt(balloons.length()), -1));
    	setupBalloon((ImageView)findViewById(R.id.balloon_18), r, 
    			balloons.getResourceId(r.nextInt(balloons.length()), -1));
    	setupBalloon((ImageView)findViewById(R.id.balloon_19), r, 
    			balloons.getResourceId(r.nextInt(balloons.length()), -1));
    	setupBalloon((ImageView)findViewById(R.id.balloon_20), r, 
    			balloons.getResourceId(r.nextInt(balloons.length()), -1));
    	setupBalloon((ImageView)findViewById(R.id.balloon_21), r, 
    			balloons.getResourceId(r.nextInt(balloons.length()), -1));
    	setupBalloon((ImageView)findViewById(R.id.balloon_22), r, 
    			balloons.getResourceId(r.nextInt(balloons.length()), -1));
    	setupBalloon((ImageView)findViewById(R.id.balloon_23), r, 
    			balloons.getResourceId(r.nextInt(balloons.length()), -1));
    	setupBalloon((ImageView)findViewById(R.id.balloon_24), r, 
    			balloons.getResourceId(r.nextInt(balloons.length()), -1));
    	setupBalloon((ImageView)findViewById(R.id.balloon_25), r, 
    			balloons.getResourceId(r.nextInt(balloons.length()), -1));
    	setupBalloon((ImageView)findViewById(R.id.balloon_26), r, 
    			balloons.getResourceId(r.nextInt(balloons.length()), -1));
    	setupBalloon((ImageView)findViewById(R.id.balloon_27), r, 
    			balloons.getResourceId(r.nextInt(balloons.length()), -1));
    	setupBalloon((ImageView)findViewById(R.id.balloon_28), r, 
    			balloons.getResourceId(r.nextInt(balloons.length()), -1));
    	setupBalloon((ImageView)findViewById(R.id.balloon_29), r, 
    			balloons.getResourceId(r.nextInt(balloons.length()), -1));
    	setupBalloon((ImageView)findViewById(R.id.balloon_30), r, 
    			balloons.getResourceId(r.nextInt(balloons.length()), -1));
    	
    	Animation anim = AnimationUtils.loadAnimation(
				BalloonFill.this, R.anim.button_slide);
		anim.setStartOffset(7000);
		ImageView imgContinue = (ImageView)findViewById(R.id.imgContinue);
		imgContinue.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		imgContinue.startAnimation(anim);
	}
	
	void setupBalloon(ImageView img, Random r, int resId){
		img.setImageResource(resId);
		img.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				v.setVisibility(View.GONE);
				mGame.playSound(mGame.SOUND_RESID_balloon_pop);
			}
		});
		Animation anim = AnimationUtils.loadAnimation(
    					BalloonFill.this, R.anim.float_up_2);
		anim.setStartOffset(r.nextInt(1500));
		img.startAnimation(anim);
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.balloon_fill);
	}

}
