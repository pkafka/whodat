package com.sadboy.whodat;

import java.util.ArrayList;
import java.util.Random;

import android.app.Activity;
import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewStub;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Scroller;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.sadboy.whodat.AppMessage.AppMessageType;
import com.sadboy.whodat.Family.WhodatChar;
import com.sadboy.whodat.GameInfo.GameStatus;
import com.sadboy.whodat.GameInfo.GameTurn;
import com.sadboy.whodat.GameInfo.GameType;

public class GameGallery extends WhodatBase {

	protected static final int DIALOG_QUESTION_ANSWER = 1;
	protected static final int DIALOG_QUESTION_ASK = 2;
	protected static final int DIALOG_FINAL_GUESS = 3;
	protected static final int DIALOG_QUIT = 10;
	
	private ArrayList<WhodatChar> mComputerFlipped = 
		new ArrayList<WhodatChar>();
	
	private int[] mQuestionsComputerAsked = new int[0];
	
	private ArrayList<CardToFlip> mCards = 
		new ArrayList<GameGallery.CardToFlip>();
    
    ImageView mBtnGo;
    
	int mQuestionToAnswer;
	ImageView mQuestionImage;
	
    private ListView mConversationView;
    
    private ImageView mChatHandle1;
    private ImageView mChatHandle2;
    private EditText mMessage;
    
    @Override
    protected void onNewIntent(Intent intent) {
    	super.onNewIntent(intent);
    }
    
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
    	super.onWindowFocusChanged(hasFocus);
    	if (!hasFocus)return;
    	mBtnGo.startAnimation(getFadeAnim());
    	if (mChatHandle1 != null){
	    	mChatHandle1.startAnimation(getFadeAnim());
	    	mChatHandle2.startAnimation(getFadeAnim());
    	}
    	long since = System.currentTimeMillis() - mLastTouched;
    	if (since < 1500){
    		showTip(nextTip());
    	}
    }
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mLastTouched = System.currentTimeMillis();
		mGame.gameGallery = this;
		setContentView(R.layout.game_gallery);
		mGame.status = GameStatus.PLAYING;
		
		setupViews();
		
		ViewStub stub = (ViewStub)findViewById(R.id.stubChat);
		final ViewSwitcher chatFlipper = (ViewSwitcher)stub.inflate();
		if (mGame.type != GameType.TWO_PLAYER) chatFlipper.setVisibility(View.GONE);
		else{
			mChatHandle1 = (ImageView)chatFlipper.findViewById(R.id.chatHandleClosed);
			mChatHandle1.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					chatFlipper.showNext();
				}
			});
			mChatHandle2 = (ImageView)chatFlipper.findViewById(R.id.chatHandleOpen);
			mChatHandle2.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					chatFlipper.showNext();
				}
			});
			mMessage = (EditText)chatFlipper.findViewById(R.id.txtSend);
			ImageView imgSend = (ImageView)chatFlipper.findViewById(R.id.imgSend);
			imgSend.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					mGame.sendMessage(new AppMessage(
							mMessage.getText().toString(), 
							AppMessageType.USER_MESSAGE));
					mMessage.setText(null);
				}
			});
			chatFlipper.showNext();
			
			mConversationView = (ListView)findViewById(R.id.lvChat);
			mConversationView.setAdapter(mGame.btManager.conversationArrayAdapter);
		}
		
		mBtnGo = (ImageView)findViewById(R.id.btnGo);
		if (mGame.type != GameType.ONE_PLAYER) mBtnGo.setVisibility(View.GONE);
		else {
			mBtnGo.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					performHapticFeedback();
					if (mGame.status == GameStatus.GAME_OVER) return;
					else if (mGame.turn == GameTurn.MINE){
						Intent i = new Intent(GameGallery.this, QuestionAsk.class);
						startActivityForResult(i, DIALOG_QUESTION_ASK);
					}
					else if (mGame.turn == GameTurn.OPPONENTS){
						if (hasComputerAsked(mQuestionToAnswer) ||
								mQuestionToAnswer == 0){
							TypedArray ar = getResources().obtainTypedArray(R.array.questions_image);
							mQuestionToAnswer = ar.getResourceId(new Random().nextInt(ar.length()), 0);
							int count = 0;
							while (hasComputerAsked(mQuestionToAnswer) || count > 50) {
								mQuestionToAnswer = ar.getResourceId(new Random().nextInt(ar.length()), 0);
							}
							if (count >= 50)
							{
								Toast.makeText(GameGallery.this, 
										"all questions were asked, something went wrong...", 
										Toast.LENGTH_LONG).show();
							}
							ar.recycle();
						}
						Intent i = new Intent(GameGallery.this, QuestionAnswer.class);
						i.putExtra(QuestionAnswer.QUESTION_TO_ANSWER, mQuestionToAnswer);
						startActivityForResult(i, DIALOG_QUESTION_ANSWER);
					}
				}
			});
		}
		
		ImageView btnFinal = (ImageView)findViewById(R.id.imgBtnFinalGuess);
		if (mGame.type == GameType.CLASSIC) btnFinal.setVisibility(View.GONE);
		else {
        	btnFinal.setOnClickListener(new OnClickListener() {
    			@Override
    			public void onClick(View v) {
    				if (mGame.status == GameStatus.GAME_OVER) return;
    				performHapticFeedback();
    				
    				int[] unFlipped = new int[mCards.size()];
    				for (int i = 0; i < mCards.size(); i++) {
    					CardToFlip card = mCards.get(i);
    					if (!card.flipped) unFlipped[i] = card.resId;
    				}
    				
					if (mGame.turn == GameTurn.MINE){
						Intent i = new Intent(GameGallery.this, FinalGuess.class);
						i.putExtra(FinalGuess.UNFLIPPED, unFlipped);
						startActivityForResult(i, DIALOG_FINAL_GUESS);
					}
					else Toast.makeText(GameGallery.this, 
							"Not your turn", Toast.LENGTH_SHORT).show();
    			}
    		});
        }
        
		ImageView imgMine = (ImageView)findViewById(R.id.imgMine);
        imgMine.setImageResource(mGame.chosenCharacter);
        imgMine.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(GameGallery.this, CharacterDetail.class);
				i.putExtra(CharacterDetail.CHARACTER_ID, mGame.chosenCharacter);
				startActivity(i);
			}
		});
        
        HorizontalScrollView h = (HorizontalScrollView)findViewById(R.id.HorizontalScroll1);
        Scroller s = new Scroller(this, 
				AnimationUtils.loadInterpolator(this, 
						android.R.anim.linear_interpolator));
		//tv.setScroller(s);
		//s.startScroll(0, -200, 0, 900, 22000);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch(requestCode){
		case DIALOG_QUESTION_ASK:
			if (resultCode == Activity.RESULT_OK){
				doQuestionResults(data.getIntExtra(QuestionAsk.QUESTION_ASKED, 0));
				mGame.turn = GameTurn.OPPONENTS;
			}
			break;
		case DIALOG_QUESTION_ANSWER:
			if (resultCode != QuestionAnswer.RESULTS_NO &&
					resultCode != QuestionAnswer.RESULTS_YES) return;
			addToComputerQuestionsAked(mQuestionToAnswer);
			if (resultCode == QuestionAnswer.RESULTS_NO){
            	processComputersQuestion(GameGallery.this, mQuestionToAnswer, false);
			}
			else {
				processComputersQuestion(GameGallery.this, mQuestionToAnswer, true);
			}
			mGame.turn = GameTurn.MINE;
			break;
		case DIALOG_FINAL_GUESS:
			if (resultCode == Activity.RESULT_OK){
				if (mGame.oponentsCharacter == 
					data.getIntExtra(FinalGuess.QUESTION_ASKED, -1)){
					doWin(true);
				}
				else doLose(true);
			}
			break;
		case DIALOG_QUIT:
			if (resultCode == Quit.RESULTS_YES){
				mGame.status = GameStatus.GAME_OVER;
				mGame.sendMessage(new AppMessage("I Quit!", AppMessageType.OPPONENT_FORFEIT));
				mGame.gameGallery = null;
				finish();
			}
		}
	}
	
	void doQuestionResults(final int questionId){
		new Thread(new Runnable() {
		    public void run() {
		    	TypedArray charsWithFeatuer = mGame.getCharacters(questionId);
				boolean answer = mGame.hasFeature(mGame.oponentsCharacter, charsWithFeatuer);
		    	for (int i = 0; i < mCards.size(); i++) {
					final CardToFlip card = mCards.get(i);
					if (card.flipped) continue;
					for (int j = 0; j < charsWithFeatuer.length(); j++) {
						if (card.resId == charsWithFeatuer.getResourceId(j, 1)){
							 if (!answer) {
								 try {Thread.sleep(700);} catch (InterruptedException e) {	}
								 runOnUiThread(new Runnable() {
									@Override
									public void run() {
										mGame.playSound(mGame.SOUND_RESID_bell);
										card.flipped = true; 
										applyRotation(card.switcher);
									}
								});
							 }
							break;
						}else if (j == charsWithFeatuer.length() -1){
							if (answer) {
								try {Thread.sleep(700);} catch (InterruptedException e) {	}
								runOnUiThread(new Runnable() {
									@Override
									public void run() {
										mGame.playSound(mGame.SOUND_RESID_bell);
										card.flipped = true; 
										applyRotation(card.switcher);
									}
								});
							}
						}
					}
				}
		    }
		  }).start();
	}
	
	public void doLose(boolean notifyOpponent){
    	mGame.status = GameStatus.GAME_OVER;
    	if (notifyOpponent){
    		AppMessage m = new AppMessage("You Win!", 
    			AppMessage.AppMessageType.OPPONENT_FORFEIT);
        	mGame.sendMessage(m);
    	}
    	Intent i = new Intent(GameGallery.this, GameOver.class);
    	i.putExtra(GameOver.WON, false);
		startActivity(i);
	}
	public void doWin(boolean notifyOpponent){
		mGame.status = GameStatus.GAME_OVER;
		if (notifyOpponent){
			AppMessage m = new AppMessage("You Lost!", AppMessage.AppMessageType.OPPONENT_WON);
			mGame.sendMessage(m);
		}
    	Intent i = new Intent(GameGallery.this, GameOver.class);
    	i.putExtra(GameOver.WON, true);
		startActivity(i);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		if (mGame.status == GameStatus.GAME_OVER) finish();
	}
	
	@Override
	public void onBackPressed() {
		if (mSettingsMenu != null && mSettingsMenu.isOpened()){
			mSettingsMenu.animateClose();
			return;
		}
		if (mGame.status == GameStatus.GAME_OVER) {
			super.onBackPressed();
		}
		else {
			Intent i = new Intent(GameGallery.this, Quit.class);
			startActivityForResult(i, DIALOG_QUIT);
		}
	}
	
	void setupViews(){
    	setupView((ViewStub)findViewById(R.id.stub00), 0);
    	setupView((ViewStub)findViewById(R.id.stub01), 1);
    	setupView((ViewStub)findViewById(R.id.stub02), 2);
    	setupView((ViewStub)findViewById(R.id.stub03), 3);
    	setupView((ViewStub)findViewById(R.id.stub04), 4);
    	setupView((ViewStub)findViewById(R.id.stub05), 5);
    	setupView((ViewStub)findViewById(R.id.stub06), 6);
    	setupView((ViewStub)findViewById(R.id.stub07), 7);
    	setupView((ViewStub)findViewById(R.id.stub08), 8);
    	setupView((ViewStub)findViewById(R.id.stub09), 9);
    	setupView((ViewStub)findViewById(R.id.stub10), 10);
    	setupView((ViewStub)findViewById(R.id.stub11), 11);
    	setupView((ViewStub)findViewById(R.id.stub12), 12);
    	setupView((ViewStub)findViewById(R.id.stub13), 13);
    	setupView((ViewStub)findViewById(R.id.stub14), 14);
    	setupView((ViewStub)findViewById(R.id.stub15), 15);
    	setupView((ViewStub)findViewById(R.id.stub16), 16);
    	setupView((ViewStub)findViewById(R.id.stub17), 17);
    	setupView((ViewStub)findViewById(R.id.stub18), 18);
    	setupView((ViewStub)findViewById(R.id.stub19), 19);
    	setupView((ViewStub)findViewById(R.id.stub20), 20);
    	setupView((ViewStub)findViewById(R.id.stub21), 21);
    	setupView((ViewStub)findViewById(R.id.stub22), 22);
    	setupView((ViewStub)findViewById(R.id.stub23), 23);
    	setupView((ViewStub)findViewById(R.id.stub24), 24);
    	setupView((ViewStub)findViewById(R.id.stub25), 25);
    	setupView((ViewStub)findViewById(R.id.stub26), 26);
    	setupView((ViewStub)findViewById(R.id.stub27), 27);
    }
    
    void setupView(ViewStub stub, int index){
    	View v = stub.inflate();
    	ViewSwitcher switcher = (ViewSwitcher)v.findViewById(R.id.switcher);
    	ImageView img = (ImageView)v.findViewById(R.id.imgMain);
    	TextView tv = (TextView)v.findViewById(R.id.txtName);
    	TextView tvBack = (TextView)v.findViewById(R.id.txtNameBack);
    	tv.setTypeface(mGame.fontBacklack);
    	tvBack.setTypeface(mGame.fontBacklack);
    	
    	WhodatChar c = mGame.chosenFamily.characters.get(index);
    	img.setImageResource(c.resId);
    	tv.setText(c.name);
    	tvBack.setText(c.name);
    	
    	CardToFlip card = new CardToFlip(switcher, c.resId);
    	switcher.setTag(card);
    	mCards.add(card);
    	switcher.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				CardToFlip c = (CardToFlip)v.getTag();
				c.flipped = !c.flipped;
				applyRotation((ViewSwitcher)v);
			}
		});
    	switcher.setOnLongClickListener(new OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
				CardToFlip c = (CardToFlip)v.getTag();
				Intent i = new Intent(GameGallery.this, CharacterDetail.class);
				i.putExtra(CharacterDetail.CHARACTER_ID, c.resId);
				startActivity(i);
				return true;
			}
		});
    }

    private void applyRotation(ViewSwitcher switcher) {
    	performHapticFeedback();
		mGame.playSound(mGame.SOUND_RESID_swoosh);
        // Find the center of the container
        final float centerX = switcher.getWidth() / 2.0f;
        final float centerY = switcher.getHeight() / 2.0f;
        // Create a new 3D rotation with the supplied parameter
        // The animation listener is used to trigger the next animation
        final Rotate3dAnimation rotation =
                new Rotate3dAnimation(0, 90, centerX, centerY, 310.0f, true);
        rotation.setDuration(500);
        rotation.setFillAfter(true);
        rotation.setInterpolator(new AccelerateInterpolator());
        rotation.setAnimationListener(new DisplayNextView(switcher));
        switcher.startAnimation(rotation);
    }
    private final class DisplayNextView implements Animation.AnimationListener {
    	private final ViewSwitcher mSwitcher;
        private DisplayNextView(ViewSwitcher switcher) {
        	mSwitcher = switcher;
        }
        public void onAnimationStart(Animation animation) {}
        public void onAnimationEnd(Animation animation) {
        	runOnUiThread(new FlipView(mSwitcher));
        }
        public void onAnimationRepeat(Animation animation) {}
    }
    private final class FlipView implements Runnable {
        private final ViewSwitcher mSwitcher;
        public FlipView(ViewSwitcher switcher) {
            mSwitcher = switcher;
        }
        public void run() {
            final float centerX = mSwitcher.getWidth() / 2.0f;
            final float centerY = mSwitcher.getHeight() / 2.0f;
            Rotate3dAnimation rotation = 
            	new Rotate3dAnimation(90, 0, centerX, centerY, 310.0f, false);
            mSwitcher.showNext();
            rotation.setDuration(500);
            rotation.setFillAfter(true);
            rotation.setInterpolator(new DecelerateInterpolator());
            mSwitcher.startAnimation(rotation);
        }
    }
    
    class CardToFlip{
    	ViewSwitcher switcher;
    	int resId;
    	boolean flipped;
    	public CardToFlip(ViewSwitcher s, int r){
    		switcher = s;
    		resId = r;
    	}
    }
    
    public int[] getRuledOutIfTrue(int questionId){
		switch (questionId){
			case R.drawable.question_female:
				return new int[]{ R.array.male};
			case R.drawable.question_hair_bald:
				return new int[]{ R.drawable.question_hair_black, R.drawable.question_hair_blonde,
						R.drawable.question_hair_brown, R.drawable.question_hair_curly, 
						R.drawable.question_hair_red, R.drawable.question_hair_white};
			case R.drawable.question_hair_black:
				return new int[]{ R.drawable.question_hair_bald, R.drawable.question_hair_blonde,
						R.drawable.question_hair_brown, R.drawable.question_hair_curly, 
						R.drawable.question_hair_red, R.drawable.question_hair_white};
			case R.drawable.question_hair_blonde:
				return new int[]{ R.drawable.question_hair_black, R.drawable.question_hair_bald,
						R.drawable.question_hair_brown, R.drawable.question_hair_curly, 
						R.drawable.question_hair_red, R.drawable.question_hair_white};
			case R.drawable.question_hair_brown:
				return new int[]{ R.drawable.question_hair_black, R.drawable.question_hair_bald,
						R.drawable.question_hair_blonde, R.drawable.question_hair_curly, 
						R.drawable.question_hair_red, R.drawable.question_hair_white};
			case R.drawable.question_hair_red:
				return new int[]{ R.drawable.question_hair_black, R.drawable.question_hair_bald,
						R.drawable.question_hair_blonde, R.drawable.question_hair_curly, 
						R.drawable.question_hair_brown, R.drawable.question_hair_white};
			case R.drawable.question_hair_white:
				return new int[]{ R.drawable.question_hair_black, R.drawable.question_hair_bald,
						R.drawable.question_hair_blonde, R.drawable.question_hair_curly, 
						R.drawable.question_hair_brown, R.drawable.question_hair_red};
			case R.drawable.question_male:
				return new int[]{ R.drawable.question_female};
			case R.drawable.question_skin_dark:
				return new int[]{ R.drawable.question_skin_light};
			case R.drawable.question_skin_light:
				return new int[]{ R.drawable.question_skin_dark};
			case R.drawable.question_hair_curly:
				return new int[]{ R.drawable.question_hair_bald};
				default:
					return new int[0];
		}
	}
	
	public void processComputersQuestion(GameGallery gameGallery, int questionId, boolean userAnswer){
		TypedArray charsWithFeatuer = mGame.getCharacters(questionId);
		
		boolean answer = mGame.hasFeature(mGame.chosenCharacter, charsWithFeatuer);
		if (answer != userAnswer){
			//Toast.makeText(this, "You lied!", Toast.LENGTH_LONG).show();
		}
		
		if (answer){
			int[] relatedQuestions = getRuledOutIfTrue(questionId);
			for (int i = 0; i < relatedQuestions.length; i++) {
				addToComputerQuestionsAked(relatedQuestions[i]);
			}
		}
		
		for (int i = 0; i < mGame.chosenFamily.characters.size(); i++) {
			WhodatChar notRuledOut = mGame.chosenFamily.characters.get(i);
			if (mComputerFlipped.contains(notRuledOut)) {
				continue;
			}
			
			boolean hasFeature = mGame.hasFeature(notRuledOut.resId, charsWithFeatuer);
			if (hasFeature && !answer) mComputerFlipped.add(notRuledOut);
			else if (!hasFeature && answer) mComputerFlipped.add(notRuledOut);
		}
		
		if (mComputerFlipped.size() >= mGame.chosenFamily.characters.size() -1){
			gameGallery.doLose(false);
		}
	}
	
	void addToComputerQuestionsAked(int questionId){
		int[] newList = new int[mQuestionsComputerAsked.length + 1];
		for (int i = 0; i < mQuestionsComputerAsked.length; i++) {
			newList[i] = mQuestionsComputerAsked[i];
		}
		newList[newList.length -1] = questionId;
		mQuestionsComputerAsked = newList;
	}
	boolean hasComputerAsked(int questionId){
		for (int i = 0; i < mQuestionsComputerAsked.length; i++) {
			if (mQuestionsComputerAsked[i] == questionId) return true;
		}
		return false;
	}

    
}
