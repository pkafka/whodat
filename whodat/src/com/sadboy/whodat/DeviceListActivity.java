/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sadboy.whodat;

import java.util.ArrayList;
import java.util.Set;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

/**
 * This Activity appears as a dialog. It lists any paired devices and
 * devices detected in the area after discovery. When a device is chosen
 * by the user, the MAC address of the device is sent back to the parent
 * Activity in the result Intent.
 */
public class DeviceListActivity extends Activity {
	
    // Return Intent extra
    public static String EXTRA_DEVICE_ADDRESS = "device_address";

    // Member fields
    private BluetoothAdapter mBtAdapter;
    private ProgressBar mProgressSpinner;
    private TextView mTitle;
    
    ArrayList<BluetoothDevice> devices;
    LayoutInflater mInflater;
    GameInfo mGame;
    
    View mFooterScan;
    ProgressBar mFooterScanProgress;
    
    View mFooterDiscoverable;
    ProgressBar mFooterDiscProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.device_list);
        
        mGame = (GameInfo)getApplication();
        mInflater = getLayoutInflater();

        // Set result CANCELED incase the user backs out
        setResult(Activity.RESULT_CANCELED);
        
        mProgressSpinner = (ProgressBar)findViewById(R.id.progress); 
        mTitle = (TextView)findViewById(R.id.txtTitle);
        mTitle.setTypeface(mGame.fontBacklack);

        devices = new ArrayList<BluetoothDevice>();
        mBtAdapter = BluetoothAdapter.getDefaultAdapter();
        
        // Register for broadcasts when a device is discovered
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        this.registerReceiver(mReceiver, filter);

        // Register for broadcasts when discovery has finished
        filter = new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        this.registerReceiver(mReceiver, filter);

        // Find and set up the ListView for paired devices
        final ListView lv = (ListView) findViewById(R.id.ListViewDevices);
        lv.addFooterView(createFooterViewScan());
        lv.addFooterView(createFooterViewDiscoverable());
        lv.setAdapter(new DevicesAdapter());
        lv.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View v, int arg2, long arg3) {
				if (v == mFooterScan){
					doDiscovery();
					return;
				}
				if (v == mFooterDiscoverable){
					if (mBtAdapter.getScanMode() !=
			            BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
			            Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
			            discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
			            startActivity(discoverableIntent);
			            lv.removeFooterView(v);
			        }
					return;
				}
				Toast.makeText(DeviceListActivity.this, "Connecting...", Toast.LENGTH_SHORT).show();
				// Cancel discovery because it's costly and we're about to connect
	            mBtAdapter.cancelDiscovery();

	            ViewHolder holder = (ViewHolder)v.getTag();

	            // Create the result Intent and include the MAC address
	            Intent intent = new Intent();
	            intent.putExtra(EXTRA_DEVICE_ADDRESS, holder.device.getAddress());
	            setResult(Activity.RESULT_OK, intent);
	            finish();
			}
		});

        // Get a set of currently paired devices
        Set<BluetoothDevice> pairedDevices = mBtAdapter.getBondedDevices();
        if (pairedDevices.size() > 0) {
            for (BluetoothDevice device : pairedDevices) {
                devices.add(device);
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // Make sure we're not doing discovery anymore
        if (mBtAdapter != null) {
            mBtAdapter.cancelDiscovery();
        }

        // Unregister broadcast listeners
        this.unregisterReceiver(mReceiver);
    }

    /**
     * Start device discover with the BluetoothAdapter
     */
    private void doDiscovery() {
        // Indicate scanning in the title
        mProgressSpinner.setVisibility(View.VISIBLE);
    	mFooterScanProgress.setVisibility(View.VISIBLE);
        mTitle.setText("scanning...");

        // If we're already discovering, stop it
        if (mBtAdapter.isDiscovering()) mBtAdapter.cancelDiscovery();

        // Request discover from BluetoothAdapter
        mBtAdapter.startDiscovery();
    }

    // The BroadcastReceiver that listens for discovered devices and
    // changes the title when discovery is finished
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            // When discovery finds a device
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                // Get the BluetoothDevice object from the Intent
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                // If it's already paired, skip it, because it's been listed already
                if (device.getBondState() != BluetoothDevice.BOND_BONDED) {
                    devices.add(device);
                }
            // When discovery is finished, change the Activity title
            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
            	mProgressSpinner.setVisibility(View.GONE);
            	mFooterScanProgress.setVisibility(View.GONE);
                mTitle.setText("Choose a player");
            }
        }
    };
    
    public View createFooterViewScan(){
    	mFooterScan = mInflater.inflate(R.layout.device_list_footer, null);
    	mFooterScanProgress = (ProgressBar)mFooterScan.findViewById(R.id.progress);
    	mFooterScanProgress.setVisibility(View.GONE);
    	((TextView)mFooterScan.findViewById(R.id.txtTitle)).setTypeface(mGame.fontBacklack);
    	return mFooterScan;
    }
    
    public View createFooterViewDiscoverable(){
    	mFooterDiscoverable = mInflater.inflate(R.layout.device_list_footer, null);
    	mFooterDiscProgress = (ProgressBar)mFooterDiscoverable.findViewById(R.id.progress);
    	mFooterDiscProgress.setVisibility(View.GONE);
    	TextView tv = (TextView) mFooterDiscoverable.findViewById(R.id.txtTitle);
    	tv.setTypeface(mGame.fontBacklack);
    	tv.setText("Be Discoverable...");
    	return mFooterDiscoverable;
    }
    
    
    class DevicesAdapter extends BaseAdapter {

    	
		public DevicesAdapter(){
		}
		
		@Override
		public int getCount() {
			return devices.size();
		}

		@Override
		public Object getItem(int position) {
			return devices.get(position);
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder;
	    	if (convertView == null)
	    		convertView = createView(parent);
	    		
	    	holder = (ViewHolder)convertView.getTag();
	    	
	    	BluetoothDevice d = devices.get(position);
	        holder.device = d;
	        holder.txtName.setText(d.getName());
	        return convertView;
	    }
		
		public View createView(ViewGroup parent){
	    	View v = mInflater.inflate(R.layout.device_list_row, parent, false);
	    	ViewHolder holder = new ViewHolder();
	    	holder.txtName = (TextView)v.findViewById(R.id.textView1);
	    	holder.txtName.setTypeface(mGame.fontBacklack);
	    	v.setTag(holder);
	    	return v;
	    }
		
	}
	
	static class ViewHolder{
    	BluetoothDevice device;
    	TextView txtName;
    }

}
