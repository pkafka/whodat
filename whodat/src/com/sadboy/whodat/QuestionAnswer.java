package com.sadboy.whodat;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

public class QuestionAnswer extends WhodatBase {

	public final static int RESULTS_NO = 101;
	public final static int RESULTS_YES = 201;
	public final static String QUESTION_TO_ANSWER = "QUESTION_TO_ANSWER";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.question_answer);
		
		TextView tv = (TextView)findViewById(R.id.TextView01);
		tv.setTypeface(mGame.fontBacklack);
		
		TextView tvq = (TextView)findViewById(R.id.txtQmark);
		tvq.setTypeface(mGame.fontBacklack);
		
		TextView tvYes = (TextView)findViewById(R.id.txtYes);
		tvYes.setTypeface(mGame.fontBacklack);
		tvYes.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				setResult(RESULTS_YES);
				finish();
			}
		});
		
		TextView tvNo = (TextView)findViewById(R.id.txtNo);
		tvNo.setTypeface(mGame.fontBacklack);
		tvNo.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				setResult(RESULTS_NO);
				finish();
			}
		});
		
        ImageView questionImage = (ImageView)findViewById(R.id.ImgQuestion);
        questionImage.setImageResource(getIntent().getIntExtra(QUESTION_TO_ANSWER, -1));
        
        ImageView imgMyCharacter = (ImageView)findViewById(R.id.imgMyCharacter);
        imgMyCharacter.setImageResource(mGame.chosenCharacter);
        
        
	}
}
