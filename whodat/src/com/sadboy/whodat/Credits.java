package com.sadboy.whodat;

import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.animation.AnimationUtils;
import android.widget.Scroller;
import android.widget.TextView;

public class Credits extends WhodatBase {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.credits);
		
		TextView tv = (TextView)findViewById(R.id.textView1);
		tv.setTypeface(mGame.fontBacklack);
		tv.setMovementMethod(LinkMovementMethod.getInstance());
        
        Scroller s = new Scroller(this, 
				AnimationUtils.loadInterpolator(this, 
						android.R.anim.linear_interpolator));
		tv.setScroller(s);
		s.startScroll(0, -80, 0, 290, 10000);
	}
}
