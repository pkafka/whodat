package com.sadboy.whodat;

import android.app.Activity;
import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.TextView;

public class QuestionAsk extends WhodatBase  {

	public final static String QUESTION_ASKED = "QUESTION_RESULT";
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.question_ask);
		
		TextView tv = (TextView)findViewById(R.id.TextView01);
		tv.setTypeface(mGame.fontBacklack);
		
		Gallery g = (Gallery)findViewById(R.id.gallery1);
		g.setAdapter(new GalleryAdapter(
				getResources().obtainTypedArray(
						R.array.questions_image)));
		g.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				
				ViewHolder holder = (ViewHolder)arg1.getTag();
				Intent i = new Intent();
				i.putExtra(QUESTION_ASKED, holder.id);
				setResult(Activity.RESULT_OK, i);
				finish();
			}
		});
	}
	
	class GalleryAdapter extends BaseAdapter {

		private TypedArray mQuestions;
		private LayoutInflater mInflater;
		
		public GalleryAdapter(TypedArray questions){
	        mInflater = getLayoutInflater();
	        mQuestions = questions;
		}
		
		@Override
		public int getCount() {
			return mQuestions.length();
		}

		@Override
		public Object getItem(int position) {
			return mQuestions.getResourceId(position, -1);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			
			ViewHolder holder;
	    	if (convertView == null)
	    		convertView = createView(parent);
	    		
	    	holder = (ViewHolder)convertView.getTag();
	        holder.id = mQuestions.getResourceId(position, -1);
	        holder.imgMain.setImageResource(holder.id);
	        
	        return convertView;
	    }
		
		public View createView(ViewGroup parent){
	    	View v = mInflater.inflate(R.layout.gallery_item, parent, false);
	    	ViewHolder holder = new ViewHolder();
	    	holder.imgMain = (ImageView)v.findViewById(R.id.imgMain); 
	    	holder.imgMain.setBackgroundResource(R.xml.character_background);
	    	v.setTag(holder);
	    	return v;
	    }
		
	}
	
	static class ViewHolder{
    	ImageView imgMain;
    	int id;
    }
	
}
