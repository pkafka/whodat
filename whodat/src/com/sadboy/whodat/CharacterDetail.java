package com.sadboy.whodat;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Scroller;
import android.widget.TextView;

public class CharacterDetail extends WhodatBase {
	
	public static final String CHARACTER_ID = "CHARACTER_ID";
	
	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		if (hasFocus){
			TextView tv = (TextView)findViewById(R.id.textView1);
			Scroller s = new Scroller(this, 
					AnimationUtils.loadInterpolator(this, 
							android.R.anim.linear_interpolator));
			//tv.setScroller(s);
			//s.startScroll(0, -200, 0, 900, 22000);
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.character_detail);
		
	    int characterId = getIntent().getIntExtra(CHARACTER_ID, -1);
	    ImageView img = (ImageView)findViewById(R.id.imageView1);
	    img.setImageResource(characterId);
		
		TextView tv = (TextView)findViewById(R.id.textView1);
		tv.setTypeface(mGame.fontBacklack);
		tv.setText(getBio(characterId));
		
		RelativeLayout r = (RelativeLayout)findViewById(R.id.RelativeLayout01);
		r.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});

	}
	
	public String getBio(int character){
		switch (character){
			case R.drawable.character_00_00:
				return getResources().getString(R.string.bio_00_00);
			case R.drawable.character_00_01:
				return getResources().getString(R.string.bio_00_01);
			case R.drawable.character_00_02:
				return getResources().getString(R.string.bio_00_02);
			case R.drawable.character_00_03:
				return getResources().getString(R.string.bio_00_03);
			case R.drawable.character_00_04:
				return getResources().getString(R.string.bio_00_04);
			case R.drawable.character_00_05:
				return getResources().getString(R.string.bio_00_05);
			case R.drawable.character_00_06:
				return getResources().getString(R.string.bio_00_06);
			case R.drawable.character_00_07:
				return getResources().getString(R.string.bio_00_07);
			case R.drawable.character_00_08:
				return getResources().getString(R.string.bio_00_08);
			case R.drawable.character_00_09:
				return getResources().getString(R.string.bio_00_09);
			case R.drawable.character_00_10:
				return getResources().getString(R.string.bio_00_10);
			case R.drawable.character_00_11:
				return getResources().getString(R.string.bio_00_11);
			case R.drawable.character_00_12:
				return getResources().getString(R.string.bio_00_12);
			case R.drawable.character_00_13:
				return getResources().getString(R.string.bio_00_13);
			case R.drawable.character_00_14:
				return getResources().getString(R.string.bio_00_14);
			case R.drawable.character_00_15:
				return getResources().getString(R.string.bio_00_15);
			case R.drawable.character_00_16:
				return getResources().getString(R.string.bio_00_16);
			case R.drawable.character_00_17:
				return getResources().getString(R.string.bio_00_17);
			case R.drawable.character_00_18:
				return getResources().getString(R.string.bio_00_18);
			case R.drawable.character_00_19:
				return getResources().getString(R.string.bio_00_19);
			case R.drawable.character_00_20:
				return getResources().getString(R.string.bio_00_20);
			case R.drawable.character_00_21:
				return getResources().getString(R.string.bio_00_21);
			case R.drawable.character_00_22:
				return getResources().getString(R.string.bio_00_22);
			case R.drawable.character_00_23:
				return getResources().getString(R.string.bio_00_23);
			case R.drawable.character_00_24:
				return getResources().getString(R.string.bio_00_24);
			case R.drawable.character_00_25:
				return getResources().getString(R.string.bio_00_25);
			case R.drawable.character_00_26:
				return getResources().getString(R.string.bio_00_26);
			case R.drawable.character_00_27:
				return getResources().getString(R.string.bio_00_27);
			default:
				return getResources().getString(R.string.bio_default);
		}
	}
	
}
