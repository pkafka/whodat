package com.sadboy.whodat;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.TextView;

import com.sadboy.whodat.Family.WhodatChar;

public class FinalGuess extends WhodatBase {

	public final static String QUESTION_ASKED = "GUESS_RESULT";
	public final static String UNFLIPPED = "UNFLIPPED";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.final_guess);

		TextView tv = (TextView)findViewById(R.id.TextView01);
		tv.setTypeface(mGame.fontBacklack);
		
		int[] characters = getIntent().getIntArrayExtra(UNFLIPPED);
		ArrayList<WhodatChar> unflipped = new ArrayList<Family.WhodatChar>();
		for (int i = 0; i < characters.length; i++){
			if (characters[i] == 0)continue;
			unflipped.add(mGame.chosenFamily.characters.get(i));
		}
		
	    GalleryAdapter adapter = new GalleryAdapter(
	    		FinalGuess.this, 
	    		unflipped);
		
		final Gallery g1 = (Gallery)findViewById(R.id.gallery1);
		g1.setAdapter(adapter);
		g1.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView parent, View v, int position, long id) {
            	performHapticFeedback();
                ViewHolder holder = (ViewHolder)v.getTag();
				Intent i = new Intent();
				i.putExtra(QUESTION_ASKED, holder.id);
				setResult(Activity.RESULT_OK, i);
				finish();
            }
        });
		
	}
	
	class GalleryAdapter extends BaseAdapter {

		private LayoutInflater mInflater;
		private ArrayList<WhodatChar> mUnflipped;
		
		public GalleryAdapter(Context c, ArrayList<WhodatChar> unflipped){
	        mInflater = LayoutInflater.from(c);
			mUnflipped = unflipped;
		}
		
		@Override
		public int getCount() {
			return mUnflipped.size();
		}

		@Override
		public Object getItem(int position) {
			return mUnflipped.get(position);
		}

		@Override
		public long getItemId(int position) {
			return mUnflipped.get(position).resId;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder;
	    	if (convertView == null)
	    		convertView = createView(parent);
	    		
	    	holder = (ViewHolder)convertView.getTag();
	    	
	    	WhodatChar c = mUnflipped.get(position);
	        holder.id = c.resId;
	        holder.imgMain.setImageResource(holder.id);
	        holder.txtName.setText(c.name);
	        holder.txtNameBack.setText(c.name);
	        return convertView;
	    }
		
		public View createView(ViewGroup parent){
	    	View v = mInflater.inflate(R.layout.character, parent, false);
	    	ViewHolder holder = new ViewHolder();
	    	holder.imgMain = (ImageView)v.findViewById(R.id.imgMain); 
	    	holder.txtName = (TextView)v.findViewById(R.id.txtName);
	    	holder.txtNameBack = (TextView)v.findViewById(R.id.txtNameBack);
	    	holder.txtName.setTypeface(mGame.fontBacklack);
	    	holder.txtNameBack.setTypeface(mGame.fontBacklack);
	    	v.setTag(holder);
	    	return v;
	    }
		
	}
	
	static class ViewHolder{
    	ImageView imgMain;
    	TextView txtName;
    	TextView txtNameBack;
    	int id;
    }
	
}
